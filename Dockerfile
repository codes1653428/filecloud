FROM filecloud/fileclouddocker

# Map the required ports
EXPOSE 443 80

# Set up volume definitions
VOLUME /data/db
VOLUME /data/files
VOLUME /data/solr

# Set container restart policy
CMD ["--restart", "always"]
